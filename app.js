/**
 * Summary (Plantilla app)
 *
 * Description. (Plantilla app)
 *
 * @class
 * @author Jorge Veliz
 * @since  1.0.0
 */
const express = require('express')
const app = express()
const { logger } = require('./services/logger')
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(bodyParser.json())

/**
 *
 * Description. (peticion inicial)
 *
 * @since      1.0.0
 * @param {Tipo} name   Descripcion
*/
app.get('/', (req, res) => {
  logger.log({
    level: 'info',
    class: 'index',
    message: 'peticion inicial'
  })
  res.statusCode = 200
  res.end('Hello world')
})

/**
 *
 * Description. (peticion de lanzar dado)
 *
 * @since      1.0.0
 * @Param {Int} dados (cantidad de dados a lanzar)
*/
app.get('/tirar/:dados', (req, res) => {
  logger.log({
    level: 'info',
    class: 'app',
    message: 'peticion lanzar dados recibida'
  })
  var queryParameter = req.params
  // Object.prototype.hasOwnProperty.call(queryParameter, 'dados')
  if (Number(queryParameter.dados)) {
    const dadosArray = []
    for (let index = 0; index < Number(queryParameter.dados); index++) {
      dadosArray.push(lanzarDado())
    }
    logger.log({
      level: 'info',
      class: 'index',
      message: 'la peticion de lanzar dados ha terminado, los dados devueltos son ' + dadosArray
    })
    res.status(200).send({ dados: dadosArray })
  } else {
    logger.log({
      level: 'error',
      class: 'app',
      message: 'peticion lanzar dados erronea, no posee parametros adecuados'
    })
    res.status(400).send('Número de dados a tirar no es válido')
  }
})

/**
 *
 * Description. (devuelve un numero aleatorio [1-6] simulando un dado)
 *
 * @since      1.0.0
*/
function lanzarDado () {
  var num = Math.random() * 6
  return Math.ceil(num)
}

module.exports = app
