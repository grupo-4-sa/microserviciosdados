# Microservicio Dados

_APIS elaborado en NodeJS en su versión 11.12.0 la estructura básica está en el archivo index.js y en el archivo app.js se encuentra toda la logica de los servicios bajo los estandares establecidos de comunicacion_

## Información General
- Proyecto de Laboratorio Software Avanzado
- Creado por Jorge Veliz
- Carnet 201314285
- Octubre 2020


## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Instalación 🔧

_La instalación es muy sencilla, se debe de contar con una terminal en la carpeta raiz del proyecto y luego la instalacion con NodeJS es casi instantanea_

### Pre-requisitos 📋
_Debes de tener instalado los paquetes de NodeJS_
* [Version mas actualizada](https://nodejs.org/es/) - Acá puedes descargarlo

## Compilación del Servicio ⚙️
_Estando en la carpeta raiz de tu proyecto debes de seguir el siguiente paso_
Manualmente hay que correr el siguiente comando
```
npm install
``` 

## Ejecución del Servicio
_Estando en la carpeta raiz de tu proyecto debes de seguir el siguiente paso_
Manualmente hay que correr el siguiente comando

_Servicio de Clientes, correrá en el puerto 5050_

```
npm start
``` 